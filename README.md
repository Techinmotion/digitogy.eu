# Why the Xbox Series S is Such Great Value #

Microsoft recently announced that the downgraded version of their next gen consoles is going to be priced at an astonishing $299. The Xbox Series S offers those with smaller budgets the ability to pick up a system that can play the same games as the Series X and is just a little less powerful.
The price for the Series X has not been confirmed, but it is heavily rumoured to be around $499. So for $200 less, you are getting a next gen console that can handle the latest games easily. 

The only differences between the two are slightly less processing power, the size, no media drive for the Series S and the Series X can be rendered at 4k resolution. Oh, and a smaller hard drive (though there is a bay where you can add an external hard drive anyway).
So unless you are flush with cash or a serious gamer that just has to have the best that money can buy, the Xbox Series S is an excellent value for money console. Whichever you choose, November is going to be a busy month for Xbox fans. The only bad news is that Halo, one of their most popular exclusive game franchises will not be released until 2021.
If you are unsure of which to get, we suggest reading in depth reviews so that you can compare specs and features.  An excellent resource for the latest tech news and reviews is [https://digitogy.eu/](https://digitogy.eu/) but you will find many others online too.
